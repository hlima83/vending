package com.dexma.vending;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.dexma.vending.enums.Coin;
import com.dexma.vending.enums.Product;
import com.dexma.vending.exceptions.NotAvailableException;
import com.dexma.vending.exceptions.NotEnoughChangeException;
import com.dexma.vending.exceptions.NotEnoughCreditException;
import com.dexma.vending.exceptions.NotSelectedException;
import com.dexma.vending.objects.MachineReturn;

public class VendingMachineTest {
	
	@Spy
	VendingMachineImpl vendingMachine;
	
	Map<Coin, Integer> coinInventory1;
	Map<Coin, Integer> coinInventory2;
	
	Map<Product, Integer> productInventory;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		vendingMachine = new VendingMachineImpl();
		vendingMachine = Mockito.spy(vendingMachine);
		
		coinInventory1 = new TreeMap<Coin, Integer>();
		coinInventory1.put(Coin.FIFTY_CENT, 1);
		coinInventory1.put(Coin.TWENTY_CENT, 2);
		
		coinInventory2 = new TreeMap<Coin, Integer>();
		coinInventory2.put(Coin.FIFTY_CENT, 2);
		coinInventory2.put(Coin.TEN_CENT, 1);
		
		productInventory = new HashMap<Product, Integer>();
		productInventory.put(Product.COKE, 5);
		productInventory.put(Product.PEPSI, 5);
		productInventory.put(Product.WATER, 5);
	}
	
	@Test
	public void testInitEmptyVendingMachine() {
		VendingMachine vendingMachine = new VendingMachineImpl();
		Assert.assertNull(vendingMachine.getSelectedProduct());
		Assert.assertEquals(vendingMachine.getCurrentCredit(), new Double(0));
		Assert.assertEquals(vendingMachine.getTotalSales(), new Double(0));
	}
	
	@Test(expected=NotAvailableException.class)
	public void testSelectProductNotAvailable() {
		Map<Product, Integer> productInventory = new HashMap<Product, Integer>();
		productInventory.put(Product.COKE, 5);
		productInventory.put(Product.PEPSI, 0);
		productInventory.put(Product.WATER, 10);
		
		VendingMachineImpl vendingMachine = new VendingMachineImpl(new TreeMap<Coin, Integer>(), productInventory);
		vendingMachine.selectProduct(Product.PEPSI);
	}
	
	@Test
	public void testSelectProductAvailable() {
		Map<Product, Integer> productInventory = new HashMap<Product, Integer>();
		productInventory.put(Product.COKE, 5);
		productInventory.put(Product.PEPSI, 0);
		productInventory.put(Product.WATER, 10);
		
		VendingMachineImpl vendingMachine = new VendingMachineImpl(new TreeMap<Coin, Integer>(), productInventory);
		Double productValue = vendingMachine.selectProduct(Product.COKE);
		
		Assert.assertNotNull(productValue);
		Assert.assertEquals(productValue, Product.COKE.getValue());
		Assert.assertNotNull(vendingMachine.getSelectedProduct());
		Assert.assertEquals(vendingMachine.getSelectedProduct(), Product.COKE);
	}
	
	@Test
	public void testInsertCoin() {
		VendingMachineImpl vendingMachine = new VendingMachineImpl();
		vendingMachine.insertCoin(Coin.FIFTY_CENT);
		vendingMachine.insertCoin(Coin.FIFTY_CENT);
		
		Assert.assertTrue(vendingMachine.getInsertedCoins().get(Coin.FIFTY_CENT).equals(2));
		Assert.assertEquals(vendingMachine.getCurrentCredit(), new Double(1));
	}
	
	@Test(expected=NotSelectedException.class)
	public void testCollectProductNotSelected() {
		VendingMachineImpl vendingMachine = new VendingMachineImpl();
		vendingMachine.collectProduct();
	}
	
	@Test(expected=NotEnoughCreditException.class)
	public void testCollectProductWithNotEnoughCredit() {
		vendingMachine = new VendingMachineImpl();
		vendingMachine = Mockito.spy(vendingMachine);
		
		Mockito.when(vendingMachine.getSelectedProduct()).thenReturn(Product.COKE);
		Mockito.when(vendingMachine.getCurrentCredit()).thenReturn(new Double(1.2));
		vendingMachine.collectProduct();
	}
	
	@Test(expected=NotEnoughChangeException.class)
	public void testCollectProductWithNotEnoughChange() {
		vendingMachine = new VendingMachineImpl(coinInventory1, productInventory);
		vendingMachine = Mockito.spy(vendingMachine);
		
		Mockito.when(vendingMachine.getSelectedProduct()).thenReturn(Product.WATER);
		Mockito.when(vendingMachine.getCurrentCredit()).thenReturn(new Double(2));
		vendingMachine.collectProduct();
	}
	
	@Test
	public void testCollectProductWithEnoughChange() {
		vendingMachine = new VendingMachineImpl(coinInventory2, productInventory);
		vendingMachine = Mockito.spy(vendingMachine);
		
		Mockito.when(vendingMachine.getSelectedProduct()).thenReturn(Product.WATER);
		Mockito.when(vendingMachine.getCurrentCredit()).thenReturn(new Double(2));
		
		MachineReturn machineReturn = vendingMachine.collectProduct();
		Assert.assertEquals(machineReturn.getProduct(), Product.WATER);
		Assert.assertTrue(machineReturn.getCoins().get(Coin.FIFTY_CENT).equals(2));
		Assert.assertTrue(machineReturn.getCoins().get(Coin.TEN_CENT).equals(1));
		Assert.assertEquals(vendingMachine.getTotalSales(), new Double(0.90));
	}
	
	@Test
	public void testCancelRequest() {
		vendingMachine = new VendingMachineImpl();
		vendingMachine = Mockito.spy(vendingMachine);
		
		Mockito.when(vendingMachine.getSelectedProduct()).thenReturn(Product.WATER);
		Mockito.when(vendingMachine.getInsertedCoins()).thenReturn(coinInventory1);
		
		MachineReturn machineReturn = vendingMachine.cancelRequest();
		Assert.assertTrue(machineReturn.getCoins().get(Coin.FIFTY_CENT).equals(1));
		Assert.assertTrue(machineReturn.getCoins().get(Coin.TWENTY_CENT).equals(2));
		Assert.assertNull(machineReturn.getProduct());
	}
	
	@Test
	public void testResetMachine() {
		VendingMachine vendingMachine = new VendingMachineImpl();
		vendingMachine.resetMachine();
		
		Assert.assertNull(vendingMachine.getSelectedProduct());
		Assert.assertEquals(vendingMachine.getTotalSales(), new Double(0));
		Assert.assertEquals(vendingMachine.getCurrentCredit(), new Double(0));
		
		Assert.assertTrue(vendingMachine.getCoinInventory().get(Coin.ONE).equals(0));
		Assert.assertTrue(vendingMachine.getInsertedCoins().get(Coin.TEN_CENT).equals(0));
		Assert.assertTrue(vendingMachine.getProductInventory().get(Product.PEPSI).equals(0));
		Assert.assertTrue(vendingMachine.getSoldProducts().get(Product.COKE).equals(0));
	}
}
