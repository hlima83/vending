package com.dexma.vending.objects;

import java.util.Map;

import com.dexma.vending.enums.Coin;
import com.dexma.vending.enums.Product;

public class MachineReturn {
	private Map<Coin, Integer> coins;
	private Product product;
	
	public MachineReturn(Map<Coin, Integer> coins, Product product) {
		this.coins = coins;
		this.product = product;
	}
	
	public Map<Coin, Integer> getCoins() {
		return coins;
	}
	public void setCoins(Map<Coin, Integer> coins) {
		this.coins = coins;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
		
}
