package com.dexma.vending.exceptions;

import com.dexma.vending.enums.Product;



public class NotAvailableException extends RuntimeException {
	
	public NotAvailableException(Product product) {
		super("Product" + product.toString() + " is not available!");
	}
}
