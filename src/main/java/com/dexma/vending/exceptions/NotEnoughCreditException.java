package com.dexma.vending.exceptions;

import com.dexma.vending.enums.Product;


public class NotEnoughCreditException extends RuntimeException {
	
	public NotEnoughCreditException(Product product, Double credit) {
		super("You don't have enough credit to collect the selected product. Please insert " + (product.getValue() - credit));
	}
}
