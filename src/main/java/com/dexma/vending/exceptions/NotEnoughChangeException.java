package com.dexma.vending.exceptions;


public class NotEnoughChangeException extends RuntimeException {
	
	public NotEnoughChangeException() {
		super("There is not enough change in the machine");
	}
}
