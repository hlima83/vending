package com.dexma.vending.exceptions;


public class NotSelectedException extends RuntimeException {
	
	public NotSelectedException() {
		super("Please select a product");
	}
}
