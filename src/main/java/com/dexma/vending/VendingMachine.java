package com.dexma.vending;

import java.util.Map;

import com.dexma.vending.enums.Coin;
import com.dexma.vending.enums.Product;
import com.dexma.vending.objects.MachineReturn;

public interface VendingMachine {	
	
	public Map<Coin, Integer> getCoinInventory();
	public Map<Product, Integer> getProductInventory();
	public Map<Product, Integer> getSoldProducts();
	public Map<Coin, Integer> getInsertedCoins();
	public Double getCurrentCredit();
	public Product getSelectedProduct();
	public Double getTotalSales();
	
	public Double selectProduct(Product product);
	public void insertCoin(Coin coin);
	public MachineReturn collectProduct();	
	public MachineReturn cancelRequest();
	public void resetMachine();
}
