package com.dexma.vending;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.dexma.vending.enums.Coin;
import com.dexma.vending.enums.Product;
import com.dexma.vending.exceptions.NotAvailableException;
import com.dexma.vending.exceptions.NotEnoughChangeException;
import com.dexma.vending.exceptions.NotEnoughCreditException;
import com.dexma.vending.exceptions.NotSelectedException;
import com.dexma.vending.objects.MachineReturn;

public class VendingMachineImpl implements VendingMachine {
	
	private static final Integer INIT_PROD_COUNT = 10;
	private static final Integer INIT_COIN_COUNT = 20;
	
	private Map<Coin, Integer> coinInventory;
	private Map<Product, Integer> productInventory;
	private Map<Product, Integer> soldProducts;
	
	private Map<Coin, Integer> insertedCoins;
	private Double currentCredit;
	private Product selectedProduct;
	private Double totalSales;
	
	public VendingMachineImpl() {
		init();
	}
	
	public VendingMachineImpl(Map<Coin, Integer> coins, Map<Product, Integer> products) {
		init();
		coinInventory.putAll(coins);
		productInventory.putAll(products);
	}
	
	public Map<Coin, Integer> getCoinInventory() {
		return coinInventory;
	}

	public Map<Product, Integer> getProductInventory() {
		return productInventory;
	}

	public Map<Product, Integer> getSoldProducts() {
		return soldProducts;
	}

	public Map<Coin, Integer> getInsertedCoins() {
		return insertedCoins;
	}

	public Double getCurrentCredit() {
		return currentCredit;
	}

	public Product getSelectedProduct() {
		return selectedProduct;
	}

	public Double getTotalSales() {
		return totalSales;
	}

	private void init() {
		//initialize coin inventory from higher to lower value (to calculate change)
		coinInventory = new TreeMap<Coin, Integer>((Coin c1, Coin c2) -> c2.getValue().compareTo(c1.getValue()));
		productInventory = new HashMap<Product, Integer>();
		soldProducts = new HashMap<Product, Integer>();
		insertedCoins = new HashMap<Coin, Integer>();
		
		initCoins(coinInventory);
		initCoins(insertedCoins);
		
		initProducts(productInventory);
		initProducts(soldProducts);
		
		currentCredit = 0d;
		totalSales = 0d;
	}
	
	private void initCoins(Map<Coin, Integer> coinsMap) {
		coinsMap.put(Coin.FIVE_CENT, 0);
		coinsMap.put(Coin.TEN_CENT, 0);
		coinsMap.put(Coin.TWENTY_CENT, 0);
		coinsMap.put(Coin.FIFTY_CENT, 0);
		coinsMap.put(Coin.ONE,  0);
		coinsMap.put(Coin.TWO, 0);
	}
	
	private void initProducts(Map<Product, Integer> productsMap) {
		productsMap.put(Product.COKE, 0);
		productsMap.put(Product.PEPSI, 0);
		productsMap.put(Product.WATER, 0);
	}
	
	public void fillInventory() {
		
		for(Product p : Product.values()) {
			productInventory.put(p, INIT_PROD_COUNT);
		}
		
		for(Coin c : Coin.values()) {
			coinInventory.put(c, INIT_COIN_COUNT);
		}
	}
	
	/**
	 * When the client selects a product from the vending machine
	 * @param product - selected product
	 * @return product value
	 * @exception NotAvailableException - when the product is no longer available
	*/
	public Double selectProduct(Product product) throws NotAvailableException {
		if(getProductInventory().get(product) > 0) {
			selectedProduct = product;
			return product.getValue();
		}
		
		throw new NotAvailableException(product);
	}
	
	public void insertCoin(Coin coin) {
		insertedCoins.put(coin, insertedCoins.get(coin) + 1);
		currentCredit += coin.getValue();
	}
	
	/**
	 * When the client selects a product from the vending machine
	 * @param product - selected product
	 * @return product value
	 * @exception NotAvailableException - when the product is no longer available
	*/
	public MachineReturn collectProduct() throws NotSelectedException, NotEnoughCreditException, NotEnoughChangeException {
		if(this.getSelectedProduct() == null) {
			throw new NotSelectedException();
		}
		
		if(this.getCurrentCredit().compareTo(this.getSelectedProduct().getValue()) == 0) {
			MachineReturn machineReturn = new MachineReturn(new HashMap<Coin, Integer>(), this.getSelectedProduct());
			updateInventory(new HashMap<Coin, Integer>());
			return machineReturn;
			
		} else if(this.getCurrentCredit().compareTo(this.getSelectedProduct().getValue()) > 0) {
			Map<Coin, Integer> change = getChange();
			
			if(change == null) {
				throw new NotEnoughChangeException();
			}
			
			MachineReturn machineReturn = new MachineReturn(change, this.getSelectedProduct());
			updateInventory(change);
			return machineReturn;
		} else {
			throw new NotEnoughCreditException(getSelectedProduct(), this.getCurrentCredit());
		}
	}
	
	public MachineReturn cancelRequest() {
		MachineReturn machineReturn = new MachineReturn(getInsertedCoins(), null);
		insertedCoins.clear();
		selectedProduct = null;
		return machineReturn;
	}
	
	private void updateInventory(Map<Coin, Integer> change) {
		productInventory.put(getSelectedProduct(), getProductInventory().get(getSelectedProduct()) - 1);
		soldProducts.put(getSelectedProduct(), getProductInventory().get(getSelectedProduct()) + 1);
		
		totalSales += getSelectedProduct().getValue();
		selectedProduct = null;
		
		//add inserted coins
		for (Map.Entry<Coin, Integer> c : getInsertedCoins().entrySet()) {
			coinInventory.put(c.getKey(), getCoinInventory().get(c.getKey()) + c.getValue());
		}
		
		insertedCoins.clear();
		
		//deduct coins for change
		for (Map.Entry<Coin, Integer> c : getChange().entrySet()) {
			coinInventory.put(c.getKey(), coinInventory.get(c.getKey()) - c.getValue());
		}
	}
	
	public void resetMachine() {
		initCoins(coinInventory);
		initCoins(insertedCoins);
		initProducts(productInventory);
		initProducts(soldProducts);
		
		currentCredit = 0d;
		totalSales = 0d;
		selectedProduct = null;
	}
	
	private Map<Coin, Integer> getChange() {
		Double changeValue = getCurrentCredit() - getSelectedProduct().getValue();
		Double currentChangeValue = 0d;
		Map<Coin, Integer> currentChange = new HashMap<Coin, Integer>(); 
		initCoins(currentChange);
		
		for (Map.Entry<Coin, Integer> c : getCoinInventory().entrySet()) {
			Coin currentCoin = c.getKey();
			Integer currentCoinCount = c.getValue();
			
			while(currentCoinCount > 0 && currentChangeValue + currentCoin.getValue() <= changeValue && currentCoinCount > 0) {
				currentChangeValue += currentCoin.getValue();
				currentChange.put(currentCoin, currentChange.get(currentCoin) + 1);
				currentCoinCount--;
			}
			
			if(currentChangeValue.equals(changeValue)) {
				return currentChange;
			}
		}
		
		return null;
	}
}
