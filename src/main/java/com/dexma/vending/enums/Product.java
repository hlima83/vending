package com.dexma.vending.enums;

public enum Product {
	COKE (1.5),
	PEPSI (1.45),
	WATER (0.90);
	
	private final Double value;
	
	Product(Double value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return this.name().toLowerCase();
	}
	
	public Double getValue() {
		return value;
	}
}
