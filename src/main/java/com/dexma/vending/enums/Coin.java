package com.dexma.vending.enums;

public enum Coin {
	FIVE_CENT (0.05),
	TEN_CENT (0.10),
	TWENTY_CENT (0.20),
	FIFTY_CENT (0.50),
	ONE (1d),
	TWO (2d);
	
	private Double value;
	
	Coin(Double value) {
		this.value = value;
	}
	
	public Double getValue() {
		return value;
	}
}